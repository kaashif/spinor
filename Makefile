.PHONY: notes present clean

all:
	@echo Please use either 'make notes' or 'make present'

notes:
	@echo Making handout with notes
	@cp notes.tex settings.tex
	@pdflatex spinor.tex >/dev/null
	@pdflatex faq.tex >/dev/null
	@rm settings.tex
	@pdfnup --nup 2x3 --scale 0.9 --no-landscape --delta "1cm 1cm" --frame true --noautoscale false spinor.pdf --outfile slides-notes.pdf >/dev/null
	@pdfjam slides-notes.pdf '-' faq.pdf '-' --outfile notes.pdf >/dev/null
	@rm slides-notes.pdf faq.pdf
	@echo The finished notes are in 'notes.pdf'

present:
	@echo Making slides for presentation
	@pdflatex spinor.tex >/dev/null
	@mv spinor.pdf slides.pdf
	@echo The finished slides are in 'slides.pdf'

clean:
	@echo Cleaning...
	@rm -f *.aux *.nav *.log *.snm *.toc *.out settings.tex
