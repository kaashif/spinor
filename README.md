Rotations
=========
This is a presentation for a talk on rotations, involving some topology
(not very much), quaternions and vector spaces.

Note: This may not be the most up-to-date version. Remember to check
ShareLatex.

License
-------
This work is licensed under the Creative Commons
Attribution-NonCommercial license, which you can find in the LICENSE file.
